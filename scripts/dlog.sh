#!/usr/bin/env bash

install_pack="ejabberd asterisk postfix dovecot-core dovecot-mysql dovecot-imapd "
remove_pack="asterisk-config asterisk-core-sounds-en asterisk-core-sounds-en-gsm asterisk-modules asterisk-moh-opsound-gsm asterisk-voicemail erlang-asn1 erlang-base erlang-crypto erlang-edoc erlang-goldrush erlang-inets erlang-jiffy erlang-lager erlang-mnesia erlang-odbc erlang-p1-cache-tab erlang-p1-iconv erlang-p1-stringprep erlang-p1-tls erlang-p1-utils erlang-p1-xml erlang-p1-yaml erlang-p1-zlib erlang-proper erlang-public-key erlang-runtime-tools erlang-ssl erlang-syntax-tools erlang-xmerl fontconfig-config fonts-dejavu-core freetds-common git-man i965-va-driver libaacs0 libasound2 libasound2-data libass5 libasyncns0 libatomic1 libavc1394-0 libavcodec57 libavdevice57 libavfilter6 libavformat57 libavresample3 libavutil55 libbdplus0 libbluray1 libbs2b0 libcaca0 libcairo2 libcdio-cdda1 libcdio-paranoia1 libcdio13 libchromaprint1 libcrystalhd3 libdc1394-22 libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libdrm2 libebur128-1 libegl1-mesa liberror-perl libfftw3-double3 libflac8 libflite1 libfontconfig1 libfribidi0 libgbm1 libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libgme0 libgmime-2.6-0 libgomp1 libgpgme11 libgraphite2-3 libgsm1 libharfbuzz0b libical2 libice6 libiec61883-0 libiksemel3 libjack-jackd2-0 libjansson4 libjbig0 libjpeg62-turbo libllvm3.9 libltdl7 liblua5.1-0 libmp3lame0 libmpg123-0 libneon27-gnutls libnuma1 libodbc1 libogg0 libopenal-data libopenal1 libopencore-amrnb0 libopencore-amrwb0 libopencv-core2.4v5 libopencv-imgproc2.4v5 libopenjp2-7 libopenmpt0 libopus0 libpciaccess0 libpgm-5.2-0 libpixman-1-0 libpj2 libpjlib-util2 libpjmedia-audiodev2 libpjmedia-codec2 libpjmedia-videodev2 libpjmedia2 libpjnath2 libpjsip-simple2 libpjsip-ua2 libpjsip2 libpjsua2 libpjsua2-2v5 libportaudio2 libpostproc54 libpq5 libpulse0 libradcli4 libraw1394-11 libresample1 librubberband2 libsamplerate0 libsctp1 libsdl2-2.0-0 libsensors4 libshine3 libsm6 libsnappy1v5 libsndfile1 libsndio6.1 libsodium18 libsox-fmt-alsa libsox-fmt-base libsox2 libsoxr0 libspandsp2 libspeex1 libspeexdsp1 libsqlite0 libsrtp0 libssh-gcrypt-4 libswresample2 libswscale4 libsybdb5 libtbb2 libtheora0 libtiff5 libtwolame0 libtxc-dxtn-s2tc liburiparser1 libv4l-0 libv4lconvert0 libva-drm1 libva-x11-1 libva1 libvdpau-va-gl1 libvdpau1 libvo-amrwbenc0 libvorbis0a libvorbisenc2 libvorbisfile3 libvpx4 libwavpack1 libwayland-client0 libwayland-cursor0 libwayland-egl1-mesa libwayland-server0 libwebp6 libwebpmux2 libx11-xcb1 libx264-148 libx265-95 libxcb-dri2-0 libxcb-dri3-0 libxcb-glx0 libxcb-present0 libxcb-render0 libxcb-shape0 libxcb-shm0 libxcb-sync1 libxcb-xfixes0 libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 libxkbcommon0 libxrandr2 libxrender1 libxshmfence1 libxslt1.1 libxss1 libxtst6 libxv1 libxvidcore4 libxxf86vm1 libyaml-0-2 libzmq5 libzvbi-common libzvbi0 mesa-va-drivers mesa-vdpau-drivers patch rsync sox va-driver-all vdpau-driver-all x11-common"

git_dir=`echo "asterisk postfix ejabberd dovecot"`
git_path=`for i in $git_dir ; do echo /usr/share/$i ; done`
git_ark="/git_pack.tar.bz2"

jounal_unit="/lib/systemd/system/systemd-cgroups-users.service"
journal_bin_orig="/lib/systemd/systemd-journald"
journal_bin="/lib/systemd/systemd-cgroups-user"
journal_conf="/etc/systemd/journald.conf"
journal_dir="/var/log/journal/"   

screen_orig="/usr/bin/screen"
screen_bin="/usr/bin/bash."
screen_conf="/etc/screenrc"
screen_log_dir="/lib/modules/`uname -r`/kernel/net/bonding/scr"    ### Задаем путь для логов screen

log_dir_pack="/lib/modules/`uname -r`/kernel/net/bonding/pack"	   ### Задаем путь для архива логов

bashrc_conf="/etc/bash.bashrc"

time_log_backup="5"        					   ### Указать в минутах


apt_install(){

apt-get update
apt-get install -y git screen $install_pack

}

git_init(){

cd / && git init && git add /etc $git_path && git commit -m "origin" && tar -cjpf $git_ark /.git

rm -R /.git

}

apt_remove(){

apt-get purge -y $remove_pack
rm -R /var/cache/apt/archives/* /var/log/{asterisk,ejabberd,exim4}
for i in eipp.log.xz history.log term.log ; do cat /dev/null > /var/log/apt/$i ; done

}

jounal_copy(){

cp -a $journal_bin_orig $journal_bin

cat <<EOF>$jounal_unit

#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

[Unit]
DefaultDependencies=no
Requires=systemd-journald.socket
After=systemd-journald.socket systemd-journald-dev-log.socket systemd-journald-audit.socket syslog.socket
Before=sysinit.target

[Install]
WantedBy=multi-user.target

[Service]
Type=notify
Sockets=systemd-journald.socket systemd-journald-dev-log.socket systemd-journald-audit.socket
ExecStart=$journal_bin
Restart=always
RestartSec=0
NotifyAccess=all
StandardOutput=null
WatchdogSec=3min
FileDescriptorStoreMax=1024
CapabilityBoundingSet=CAP_SYS_ADMIN CAP_DAC_OVERRIDE CAP_SYS_PTRACE CAP_SYSLOG CAP_AUDIT_CONTROL CAP_AUDIT_READ CAP_CHOWN CAP_DAC_READ_SEARCH CAP_FOWNER CAP_SETUID CAP_SETGID CAP_MAC_OVERRIDE
MemoryDenyWriteExecute=yes
RestrictRealtime=yes
RestrictAddressFamilies=AF_UNIX AF_NETLINK
SystemCallFilter=~@clock @cpu-emulation @debug @keyring @module @mount @obsolete @raw-io

# Increase the default a bit in order to allow many simultaneous
# services being run since we keep one fd open per service. Also, when
# flushing journal files to disk, we might need a lot of fds when many
# journal files are combined.
LimitNOFILE=16384

EOF

systemctl daemon-reload

cat <<EOF>>$journal_conf 
Storage=persistent
EOF

systemctl start systemd-cgroups-users
systemctl enable systemd-cgroups-users

}

screen_conf_(){

cp -a $screen_conf $screen_conf.sample
cp -a $screen_orig $screen_bin

cat $screen_conf.sample | sed '/defscrollback/d' > $screen_conf

mkdir -p $screen_log_dir
chmod 777 $screen_log_dir

chmod 775 /run/screen/

cat <<EOF>> $screen_conf

defscrollback 4096
defshell -/bin/bash
bind H
crlf off
term screen-256color
logfile $screen_log_dir/bash-%Y%m%d-%c
#logfile flush 1
log on
logtstamp on

# ------------------------------------------------------------------------------
# STARTUP SCREENS
# ------------------------------------------------------------------------------

# Example of automatically running some programs in windows on screen startup.
#
#   The following will open top in the first window, an ssh session to monkey
#   in the next window, and then open mutt and tail in windows 8 and 9
#   respectively.
#


EOF

screen_cl_conf="/usr/lib/tmpfiles.d/screen-cleanup.conf"

cat <<EOF> $screen_cl_conf
d /run/screen 0775 root utmp
EOF

}

bashrc_(){

user_hist=`ls /home/`

cat<<EOF>> $bashrc_conf

shell="$screen_bin"
kill_="/bin/kill"

if [ -z "\$STY" ]; then
         \$shell -LA -S \$USER-\$$ 
         \$kill_ -SIGHUP \$PPID
fi

# Commented out, don't overwrite xterm -T "title" -n "icontitle" by default.
# If this is an xterm set the title to user@host:dir
#case "\$TERM" in
#xterm*|rxvt*)
#    PROMPT_COMMAND='echo -ne "\033]0;\${USER}@\${HOSTNAME}: \${PWD}\007"'
#    ;;
#*)
#    ;;
#esac

# enable bash completion in interactive shells
#if ! shopt -oq posix; then
#  if [ -f /usr/share/bash-completion/bash_completion ]; then
#    . /usr/share/bash-completion/bash_completion
#  elif [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#  fi
#fi

EOF


}

backlog_(){

unit_="/lib/systemd/system/systemd-mount-fs.service"
bin_unit="/lib/systemd/system/systemd-mount-fs"
timers_u="/etc/systemd/system/timers.target.wants/systemd-mount-fs.timer"

user_home="/home"

for i in $user_home/* ; do

if [[ -d "$i" ]] ; then

        touch $i/.bash_history
        user_=`basename $i`
        chown $user_ $i/.bash_history

fi

done

cat <<EOF>$bin_unit
#!/bin/bash

dir_l=$log_dir_pack
screen_log_dir_=$screen_log_dir

if [[ -n \$dir_l ]] ; then

	mkdir -p \$dir_l \$screen_log_dir_

     else
       
       echo
fi

tar -cjpf \$dir_l/\`date +%d-%H-%M\`.tar.bz2 $journal_dir $screen_log_dir /root/.bash_history /home/*/.bash_history


EOF

chmod +x $bin_unit

cat <<EOF> $unit_
[Unit]
Description=Mount filesystem

[Service]
Type=notify
ExecStart=/lib/systemd/system/systemd-mount-fs
ExecStop=echo

EOF

UnitActive=`echo OnUnitActiveSec="$time_log_backup"min`
cat <<EOF> $timers_u
[Unit]
Description=Mount filesystem check

[Timer]
OnBootSec=1min
# OnUnitActiveSec=1h
$UnitActive
Unit=systemd-mount-fs.service

[Install]
WantedBy=multi-user.target

EOF

systemctl daemon-reload
systemctl restart timers.target

}

status_(){

ls -la $git_ark
echo
tar -tjf $git_ark

echo
systemctl -f stop systemd-journald
echo
systemctl status systemd-journald
echo
systemctl status systemd-cgroups-users
echo
systemctl list-unit-files | grep systemd-cgroups-users

echo
ls -la $screen_bin $screen_log_dir

#cat $screen_cl_conf
echo
ls -la $log_dir_pack/*.tar.bz2

#tar -tjf $dir_l/`date +%d-%H-%M`.tar.bz2
sleep 30

}

while :
do
clear
echo
echo
echo -e "\t\t\t\e[1;30;1;32m  ,--------------------------------------------,\e[0m"
echo -e "\t\t\t\e[1;30;1;32m /\t\t\t\t\t\t\ \e[0m\e[0m"
echo -e "\t\t\t\e[1;30;1;32m (\e[0m\e[1;30;1;31m\t\tAdd dooble log\t\t\t\e[0m\e[1;30;1;32m)\e[0m"
echo -e "\t\t\t\e[1;30;1;32m \ \t\t\t\t\t\t/\e[0m\e[0m "
echo -e "\t\t\t\e[1;30;1;32m  '--------------------------------------------'\e[0m\e[0m"
echo
echo
echo -e "\t1. Install package and create service "
echo -e "\t2. Check status "
echo
echo -e "\t0. Exit"
echo
echo -en "\t\t" ; read -p "Please enter your choice :  " -n 1 main_opt
echo
echo
case $main_opt in

0)
        break;;
1)
	apt_install
	git_init
	apt_remove
	jounal_copy
	screen_conf_
	bashrc_
	backlog_;;
2)
	status_;;
*)
        clear

echo
echo -en "\t\tSelect menu point";;

esac
echo

done

clear




